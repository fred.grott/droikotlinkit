# DroidKotlinKit

This is my fine tuned gradle build setup and gitlab ci-server config scripts for
fast prototyping Android Kotlin projects.

While I am not accepting contributiions you can download or fork this to
customize for your own use.

# Why

Why do this? All build and ci systems and their dependencies make for very complex
things that have a habit of failing when we least expect it. The only way to 
decrease such unhapy events and get a very dependable build tool is to 
debug the whole thing, wrie every workaround and detail down, and put the 
general guide docs in such a form that even a junior dev can digest them and 
understand them.

In short words, preventive medicine.


# TODO



Node Yeoman project template setup using this project as a base

My Own preconfigured docekr image for android development

Re enable pitest as soon as the problem with applying it to app modules
with other modules deps is resolved

A variant of this with a product flavors setup in the app module

# Articles and SlideDexks on Devops

To be added at some point






# Gradle

## Build File Philosophy

Due to mutliple ranges of gradle and android gradle plugin expertise, I kept the
project modules detailing the common plugins and settings as their will be use
cases where setting might have to vary per propject module and its easier to
see what each project module build script does if its not split into
gradle script as plugins approach to task customization.

Also, set it up so that its somewhat easier to customize as all custom
stuff including third party plugins and tasks is right after dependency
block.



## Warning Suppression

### Android Gradle Transform

gradle.properties jetifier blacklist as certain third party libs fail
under the jetifier transform and obviously don't have refs to android support
libs.

### Code Warnings Suppression

#### FindBugs Spotbugs

For just suppress a warning per block of code use the annotation

```
@SuppressWarnings
```
To suppress something specific use

```
@SuppressFBWarnings("URF_UNREAD_FIELD")
```

Note, most of us will get a xeres xml parse error warning and not finding 
android framework classes but than can be somewhat ignored as the
classpath entry of files will set the auxclasspath with the ocrrect 
android framework classes referenced.

Also note, FindBugs gradle plugin by gradle depreciates in gradle 6 end of 2019 
so need to track how spotbugs transitions to taking that source from 
gradle and adding their own version to their own plugin project.

#### Detekt

for classes

```
@Suppress("LargeClass") // Yes, objects are also reported by the same rule.
object Constants {
    ...
}
```

For kotlin files

```
@file:Suppress("TooManyFunctions").
```

#### AndroidLint

Just use the specific warning

```
@SuppressLint("NewApi")
```

## Reports

### modules

build

     reports

        godot
        
        tests
            bdd coverage shows here

     outputs


          dexcount

          logs

             has the manifest merge reports

          mapping

            has the R8 shrinker mappings

          reports

              unit

              spoonTests

              jacoco

              spotbugs

              lint

## Mocking

For the longest time Android Devs have been use to the make an interface and other attached dog shit to 
mock classes. So long in fact that they think that anti-pattern is not an anti-pattern.
In my setup I am using Mockk as it does not have the anti-pattern workarounds to make work and be fast like 
mockito as it just works the way Kotlin is suppose to work in a fast test way.

Usaage details are here

https://github.com/mockk/mockk

android support details here

https://mockk.io/ANDROID.html


I make use of the alloopen plugin to get access to final before 
android P as its better test performance. Details of setup is here

https://proandroiddev.com/mocking-androidtest-in-kotlin-51f0a603d500

Basically one has one extra annotation to mock something that is final

```
@OpenForTesting
```


## Spek BDD Test Setup

I use the junit4 version of spek and that specific setup. Example test in the
test subfolder of the src folder.

## TDD Additional Setup

I kept the normal non-spoon enabled connected tasks set up to use with
cloud device farms. However, I have not enabled a the optional
code and junit rule to take a screenshot like spoon does. Something to do later.

### Spoon Additional Setup

By default spoon does not handle the dialog and popup screens well. So the
additional setup is to use the Falcon Spoon helper library.

The additional is a utils kotlin object to get the current activity and
to execute a special spoontestrule to get the screenshot. Than an
example tdd unitl test with the test rule applied.

## Debug Log Additional Setup

I use two things Hugo and Timber from jake Wharton and Timberkt.

### Hugo

I am using a temp fix someone did to get hugo working with Kotlin. Its the
same exact debug annotation

```
@DebugLog
```

### Timber and Timberkt

To use the kotlin concise style

```kotlin
Timber.e(exception) { "$errorCount exceptions" }
```

or

```kotin
e(exception) { "$errorCount exceptions" }
```

One still needs to do the work of setting up a debug tree, from the
Jake Wharton example in the app class

```java
public class ExampleApp extends Application {
  @Override public void onCreate() {
    super.onCreate();

    if (BuildConfig.DEBUG) {
      Timber.plant(new DebugTree());
    } else {
      Timber.plant(new CrashReportingTree());
    }
  }

  /** A tree which logs important information for crash reporting. */
  private static final class CrashReportingTree extends Tree {
    @Override public boolean isLoggable(int priority, @Nullable String tag) {
      return priority >= INFO;
    }

    @Override protected void log(int priority, String tag, Throwable t, String message) {
      if (priority == Log.VERBOSE || priority == Log.DEBUG) {
        return;
      }

      FakeCrashLibrary.log(priority, tag, message);

      if (t != null) {
        if (priority == Log.ERROR) {
          FakeCrashLibrary.logError(t);
        } else if (priority == Log.WARN) {
          FakeCrashLibrary.logWarning(t);
        }
      }
    }
  }
}

```

and of course set up a crashlibrary class for the specific crash analytics library
you are using

```java
public final class FakeCrashLibrary {
  public static void log(int priority, String tag, String message) {
    // TODO add log entry to circular buffer.
  }

  public static void logWarning(Throwable t) {
    // TODO report non-fatal warning.
  }

  public static void logError(Throwable t) {
    // TODO report non-fatal error.
  }

  private FakeCrashLibrary() {
    throw new AssertionError("No instances.");
  }
}

```



## Okreplay 

To replay okhttp stuff in my tests. Setup and useage is here

https://github.com/airbnb/okreplay


## Okhttp Migration

set to okhttp3.13.0 to get tsl1.2 support as tsl1.1 ends at end of 2019




## Multidex

Multidex is on by default in the app model as with even the basic rxjava setup the ddxcount is at 
63k including kotlin and androdx stuff while elinmating rxjava only frees about 12k in methods. One 
more reason I am glad for being able to use flutter dart in the new FuchsiaOS coming out before 2024.


## Jacoco Setup

To get both the esspreso and the spoon coverage files, I modified the
jacoco task.

NOTE: I have set the library module jacoco task to not include TDD coverage data
as most will never have androidTest stuff in their library modules anyway


Interesting, BDD code coverage will show up in the 
pooject module build reports tests subfolder.

## Product Flavors Setup

Right now its not enabled as I have no use case for using it just yet.

Two tasks have to be modified to be dynamically denerated per the
product flavors namely jacoco and spotbugs. I do not currently
use product flavors so that is left for others to figure out if
they need it.

## Proguard R8 Shrinker Setup

Most of the stuff we originally had to do with proguard is no more as
most quality third party libs already include their own proguard
settings. One should not have to really change the default
proguard template.

## Git Tagging

While this setup uses the grgit thirdy party plugin to get git access via
Eclipse's JGIT for MS Windows to help version the app releases;
git tagging is left off as one can use the Android Studio feature
under

VCS Git Tag

## Build Properties

At times android devs have to provide web api keys and other things  which
of course we do not want those keys in the git repo. The build properties
plugin helps automaate that with cool things like fallbacks easy way to
do BuildConfigField to  pass api keys via the resValue

see this for why

https://medium.com/code-better/hiding-api-keys-from-your-android-repository-b23f5598b906

And the advance settings of the plugin are at

https://github.com/novoda/gradle-build-properties-plugin/blob/master/docs/advanced-usage.md


and a sample to see how you declare the advance settings

https://github.com/novoda/gradle-build-properties-plugin/blob/master/sample/app/build.gradle


## Crash Anakytics

I use fabric via the android studio plugin as other wise nothing downloads correctly


https://fabric.io/downloads/android-studio

after restart click left most icon on the right tool bar side and login to fabric to
define an app to get your key generated.

with adding the key to manifest dirs are here

https://fabric.io/kits/android/crashlytics/install

use the build properties plugin and the resValue so that its added right
and doesnt show up in any git repo


## DangerFile

Currently Danger, ie DangerFile. does not have complete support for GitLab CE so 
leaving off how to implement it for private projects.



## Gradle Task Sequence On New Projects

Mainly you have to get the baselines for android lint and detekt generated first.

gradlew projectmodule:lintDebug

and for

detekt

gradlew projectmodule:detektBaseline

# GitLab CI Sever Config

All shared ci runners are prevented from running android emulator
as they do not have kvm acceessbile via the docker image laucnhed 
as its a security violation.

My shared runner example only does the build and runs BDD tests.
Indie devs should see about using the Google Firebase test lab
free plan as one can run 10 virtual or 5 real device tests for 
free daily.


# License

Copyright 2019 Fred Grott

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Resouces

[Gradle Build Guides](https://gradle.org/guides)

[Android Gradle Configure Your Build](https://developer.android.com/studio/build/)

[Kotlin on Android Guides](https://developer.android.com/kotlin/)

[Dokka doc plugin setup](https://github.com/Kotlin/dokka)

[Spoon plugin setup](https://github.com/jaredsburrows/gradle-spoon-plugin)

[Detekt plugin setup](https://github.com/arturbosch/detekt)

[Spotless plugin setup](https://github.com/diffplug/spotless)

[Gradle Properties plugin setup](https://github.com/novoda/gradle-build-properties-plugin)

[Android Artifacts plugin setup](https://github.com/StefMa/AndroidArtifacts)

[Jitpack plubin setup](https://jitpack.io/docs/ANDROID/)

[Okreplay plugin setup](https://github.com/airbnb/okreplay)

[Grgit plugin setup](http://ajoberstar.org/grgit/index.html)

[Dexcount plugin setup](https://github.com/KeepSafe/dexcount-gradle-plugin)

[Godot plugin setup](https://github.com/hannesstruss/godot)

[Spotbugs plugin setup](https://spotbugs.readthedocs.io/en/stable/gradle.html)

